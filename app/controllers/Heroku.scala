package controllers

import org.apache.commons.codec.binary.Base64
import play.api.libs.json.Json
import play.api.mvc._

object Heroku extends Controller {

  def resources = HerokuAuthAction { request =>
    Ok(Json.obj("id" -> "salute"))
  }

  def deprovision(id: String) = HerokuAuthAction { request =>
    Ok("ok")
  }

  def HerokuAuthAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Action { request =>
      request.headers.get("authorization").flatMap { auth =>
        auth.split(" ").drop(1).headOption.filter { encoded =>
          new String(Base64.decodeBase64(encoded)).split(":").toList match {
            case u :: p :: Nil => u == "salute" && p == "37846e2cb11ad3bc9e183c3c03a9fdee"
            case _ => false
          }
        }.map(_ => f(request))
        }.getOrElse(Unauthorized.withHeaders("WWW-Authenticate" -> """Basic realm="Salute""""))
      }
  }

}