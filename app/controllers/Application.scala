package controllers

import play.api.mvc._

import scala.util.Random

object Application extends Controller {

  private val salutation = Map("es" -> List("Hola", "Hola #", "¿Cómo estás?", "Bienvenido #"),
                               "en" -> List("Hi #", "Hi there!", "Welcome #"),
                               "de" -> List("Hallo!", "Hallo #!", "Moin", "Servus", "Moin #", "Servus #"))

  private val default = salutation("en")

  private val random = new Random()

  def salute(name: Option[String]) = Action {
    name.map { n =>
      val withName = byLanguage(None, _.contains("#"))
      Ok(random.shuffle(withName).head.replace("#", n))
    }.getOrElse {
      val withoutName = byLanguage(None, !_.contains("#"))
      Ok(random.shuffle(withoutName).head)
    }
  }

  def saluteLang(lang: String, name: Option[String]) = Action {
    name.map { n =>
      val withName = byLanguage(Some(lang), _.contains("#"))
      Ok(random.shuffle(withName).head.replace("#", n))
    }.getOrElse {
      val withoutName = byLanguage(Some(lang), !_.contains("#"))
      Ok(random.shuffle(withoutName).head)
    }
  }

  private def byLanguage(lang: Option[String], toFilter: String => Boolean): List[String] = {
    val unfiltered = lang match {
      case Some(s) => salutation.getOrElse(s, default)
      case None => salutation.values.flatten.toList
    }
    unfiltered.filter(toFilter)
  }

}