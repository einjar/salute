require 'net/http'

url = ENV['SALUTE_URL']
puts "Connecting to #{url}"

begin
  url = URI.parse(url)
  req = Net::HTTP::Get.new(url.to_s)
  puts req
  puts "#{url.host} #{url.port}"
  res = Net::HTTP.get(URI.parse('http://localhost:9000'))
  puts "Response #{res}"
rescue => e
  abort "Failed to connect to service: #{e.message}"
end
